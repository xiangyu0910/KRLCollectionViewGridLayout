//
//  main.m
//  KLCollectionLayoutsDemo
//
//  Created by Kevin Lundberg on 3/27/14.
//  Copyright (c) 2014 Lundbergsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "KLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([KLAppDelegate class]));
    }
}

// 版权属于原作者
// http://code4app.com (cn) http://code4app.net (en)
// 发布代码于最专业的源码分享网站: Code4App.com 
