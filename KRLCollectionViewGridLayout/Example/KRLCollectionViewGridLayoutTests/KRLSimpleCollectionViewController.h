//
//  KRLSimpleCollectionViewController.h
//  KLCollectionLayouts
//
//  Created by Kevin Lundberg on 3/9/14.
//  Copyright (c) 2014 Kevin Lundberg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KRLSimpleCollectionViewController : UICollectionViewController

@property (nonatomic, copy) NSArray *items;

@property (nonatomic, copy, readonly) NSSet *visibleSupplementaryViews;

@end

// 版权属于原作者
// http://code4app.com (cn) http://code4app.net (en)
// 发布代码于最专业的源码分享网站: Code4App.com 
